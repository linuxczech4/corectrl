//
// Copyright 2021 Juan Palacios <jpalaciosdev@gmail.com>
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.
// Distributed under the GPL version 3 or any later version.
//
#include "pmoverdrivexmlparser.h"

#include "core/profilepartxmlparserprovider.h"
#include "pmoverdrive.h"
#include <memory>

AMD::PMOverdriveXMLParser::PMOverdriveXMLParser() noexcept
: ControlGroupXMLParser(AMD::PMOverdrive::ItemID)
{
}

void AMD::PMOverdriveXMLParser::loadPartFrom(pugi::xml_node const &parentNode)
{
  auto node = parentNode.find_child([&](pugi::xml_node const &node) {
    return node.name() == AMD::PMOverdrive::ItemID;
  });

  takeActive(node.attribute("active").as_bool(activeDefault()));

  if (!node) {
    // Legacy control settings section might be present in the profile.
    // These section hangs from the parent node, so we must pass it
    // to load the settings on each sub-component.
    node = parentNode;
  }

  loadComponents(node);
}

bool const AMD::PMOverdriveXMLParser::registered_ =
    ProfilePartXMLParserProvider::registerProvider(
        AMD::PMOverdrive::ItemID,
        []() { return std::make_unique<AMD::PMOverdriveXMLParser>(); });
